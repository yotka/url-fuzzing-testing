"use strict";

const clc = require("cli-color");
const parseurl = require(".");
const url = require("url");

let a, b, testStringm, passed, misses;
const testAtribs = ["query", "pathname", "host", "port", "hostname", "href"];
const testCases = require("./urls.json");

testCases.forEach((testString) => {
  passed = true;
  misses = [];
  console.log("");
  try {
    a = parseurl(testString);
    b = url.parse(testString);
  } catch (ERR_INVALID_URL) {
    console.log("🤷");
  }
  console.log(testString);
  testAtribs.forEach((atrib) => {
    if (
      a[atrib] != b[atrib] &&
      !(
        (a[atrib] == null && b[atrib] == "") ||
        (a[atrib] == "" && b[atrib] == null)
      )
    ) {
      passed = false;
      misses.push({ atrib: atrib, parseurl: a[atrib], legacy: b[atrib] });
    }
  });
  if (!passed) {
    console.log(clc.red("Missmatches:"));
    console.log(misses);
  }
});
function fuzz() { // http:// %1% : %2% @ %3% : %4% / %5% ? %6%
    let len = 16;
    let scheme = ['http://', ':', '@', ':', '/', '?'];
    let dict = 'qwertyuiopasdfghjklzxcvbnm0123456789!@#$%^&*()-=_+{}|[]\\;\':"<>?,.`~';
    let output = '';

    scheme.forEach(elem => {
        output += elem;
        output += genRandStr(dict, len);
    });

    return output
}

function genRandStr(dict, len) {
    let output = '';
    for (let i = 0; i < len; i++) {
        char = dict[genRandInt(67)];
        output += urlEncode(char);
    }

    return output
}

function genRandInt(max) {
    return Math.floor(Math.random() * max);
}

function urlEncode(char) {
    let code = char.charCodeAt(0);
    code = code.toString(16);

    return ('%' + code)
}

console.log(fuzz());
/*
* parse-url
    slashes: true,
    protocol: 'http:',
    hash: '#hash',
    query: '?query=string',
    pathname: '/p/a/t/h',
    auth: 'user:pass',
    host: 'sub.host.com:8080',
    port: '8080',
    hostname: 'sub.host.com',
    password: 'pass',
    username: 'user',
    origin: 'http://sub.host.com:8080',
    href: 'http://user:pass@sub.host.com:8080/p/a/t/h?query=string#hash'

  legacy
    protocol: 'http:',
    slashes: true,
    auth: 'user:pass',
    host: 'sub.host.com:8080',
    port: '8080',
    hostname: 'sub.host.com',
    hash: '#hash',
    search: '?query=string',
    query: 'query=string',
    pathname: '/p/a/t/h',
    path: '/p/a/t/h?query=string',
    href: 'http://user:pass@sub.host.com:8080/p/a/t/h?query=string#hash'
*/
